import Swiper, { Navigation, Pagination } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

const cross = document.getElementById("cross");
const iconMenu = document.getElementById("menu");
const menu = document.querySelector(".navigation__menu");

iconMenu.addEventListener("click", () => {
  menu.style.display = "block";
});

cross.addEventListener("click", () => {
  menu.style.display = "none";
});

const swiperMobile = new Swiper(".swiper", {
  modules: [Navigation, Pagination],
  navigation: {
    nextEl: ".slide-next",
    prevEl: ".slide-prev",
  },
  breakpoints: {
    768: {
      slidesPerView: 3,
    },
  },
  slidesPerView: 1,
  centeredSlides: true,
  centeredSlidesBounds: true,
});
