import "./mainDiscoveryPage.css";
import { createElement } from "../../../scripts/createElement";
import discoveryImage from "../../../assets/images/music-titles.png";

export default function setUpMainDiscoveryPage(parent) {
  createElement({
    tag: "div",
    attributes: {
      class: "app__main app__main-discovery",
    },
    parent,
  });

  const main = document.querySelector(".app__main");

  createElement({
    tag: "h1",
    attributes: {
      class: "main__heading main__discovery-heading",
    },
    parent: main,
    content: "Discover new music",
  });

  createElement({
    tag: "div",
    attributes: {
      class: "main__block-buttons",
    },
    parent: main,
  });

  const blockButtons = document.querySelector(".main__block-buttons");

  createElement({
    tag: "button",
    attributes: {
      class: "main__button main__discovery-button",
    },
    parent: blockButtons,
    content: "Charts",
  });

  createElement({
    tag: "button",
    attributes: {
      class: "main__button main__discovery-button",
    },
    parent: blockButtons,
    content: "Songs",
  });

  createElement({
    tag: "button",
    attributes: {
      class: "main__button main__discovery-button",
    },
    parent: blockButtons,
    content: "Artists",
  });

  createElement({
    tag: "p",
    attributes: {
      class: "main__info main__discovery-info",
    },
    parent: main,
    content:
      "By joing you can benefit by listening to the latest albums released",
  });

  createElement({
    tag: "img",
    attributes: {
      class: "main__image main__image-discovery",
      src: discoveryImage,
    },
    parent,
  });
}
