import "./headerDiscoveryPage.css";
import { createElement } from "../../../scripts/createElement";

export default function setUpHeaderDiscoveryPage(parent) {
  createElement({
    tag: "div",
    attributes: {
      class: "app__header",
    },
    parent,
  });

  const header = document.querySelector(".app__header");

  createElement({
    tag: "a",
    attributes: {
      class: "header__link-discovery",
    },
    parent: header,
    content: "Simo",
  });

  createElement({
    tag: "a",
    attributes: {
      class: "header__link-discovery header__link-signPage",
      id: "join-discoverPart",
    },
    parent: header,
    content: "Join",
  });

  createElement({
    tag: "a",
    attributes: {
      class: "header__link-discovery",
    },
    parent: header,
    content: "Sign In",
  });
}
