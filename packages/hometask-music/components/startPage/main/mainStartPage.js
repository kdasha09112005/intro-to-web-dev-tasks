import "./mainStartPage.css";
import { createElement } from "../../../scripts/createElement";
import startPageWoman from "../../../assets/images/background-page-landing.png";

export default function setUpMainStartPage(parent) {
  createElement({
    tag: "div",
    attributes: {
      class: "app__main app__main-start",
    },
    parent,
  });

  const main = document.querySelector(".app__main");

  createElement({
    tag: "div",
    attributes: {
      class: "main__block-heading",
    },
    parent: main,
  });

  const mainHeading = document.querySelector(".main__block-heading");

  createElement({
    tag: "h1",
    attributes: {
      class: "main__heading",
    },
    parent: mainHeading,
    content: "Feel the music",
  });

  createElement({
    tag: "p",
    attributes: {
      class: "main__info",
    },
    parent: mainHeading,
    content: "Stream over 10 million songs with one click",
  });

  createElement({
    tag: "button",
    attributes: {
      class: "main__button",
      id: "button-join",
    },
    parent: main,
    content: "Join now",
  });

  createElement({
    tag: "img",
    attributes: {
      class: "main__image main__image-start",
      src: startPageWoman,
    },
    parent,
  });
}
