import "./headerStartPage.css";

import { createElement } from "../../../scripts/createElement";

export default function setUpHeaderStartPage(parent) {
  createElement({
    tag: "div",
    attributes: {
      class: "app__header",
    },
    parent,
  });

  const header = document.querySelector(".app__header");

  createElement({
    tag: "a",
    attributes: {
      class: "header__link",
    },
    parent: header,
    content: "Simo",
  });

  createElement({
    tag: "a",
    attributes: {
      class: "header__link header__link-featPage",
      id: "discover",
    },
    parent: header,
    content: "Discover",
  });

  createElement({
    tag: "a",
    attributes: {
      class: "header__link header__link-signPage",
      id: "join",
    },
    parent: header,
    content: "Join",
  });

  createElement({
    tag: "a",
    attributes: {
      class: "header__link",
    },
    parent: header,
    content: "Sign In",
  });
}
