import "./logo.css";

import { createElement } from "../../scripts/createElement";
import logo from "../../assets/icons/logo.svg";

export default function setUpLogo(parent) {
  createElement({
    tag: "img",
    attributes: {
      class: "header__logo",
      src: logo,
    },
    parent,
  });
}
