import "./headerJoinPage.css";

import { createElement } from "../../../scripts/createElement";

export default function setUpHeaderJoinPage(parent) {
  createElement({
    tag: "div",
    attributes: {
      class: "app__header",
    },
    parent,
  });

  const header = document.querySelector(".app__header");

  createElement({
    tag: "a",
    attributes: {
      class: "header__link-join",
    },
    parent: header,
    content: "Simo",
  });

  createElement({
    tag: "a",
    attributes: {
      class: "header__link-join header__link-featPage",
      id: "discover-joinPart",
    },
    parent: header,
    content: "Discover",
  });

  createElement({
    tag: "a",
    attributes: {
      class: "header__link-join",
    },
    parent: header,
    content: "Sign In",
  });
}
