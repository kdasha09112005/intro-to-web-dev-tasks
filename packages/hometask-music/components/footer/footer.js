import "./footer.css";
import { createElement } from "../../scripts/createElement";

export default function setUpFooter(parent) {
  createElement({
    tag: "footer",
    attributes: {
      class: "app__footer",
    },
    parent,
  });

  const footer = document.querySelector(".app__footer");

  createElement({
    tag: "p",
    attributes: {
      class: "footer__item",
    },
    parent: footer,
    content: "About Us",
  });

  createElement({
    tag: "p",
    attributes: {
      class: "footer__item",
    },
    parent: footer,
    content: "Contact",
  });

  createElement({
    tag: "p",
    attributes: {
      class: "footer__item",
    },
    parent: footer,
    content: "CR Info",
  });

  createElement({
    tag: "p",
    attributes: {
      class: "footer__item",
    },
    parent: footer,
    content: "Twitter",
  });

  createElement({
    tag: "p",
    attributes: {
      class: "footer__item",
    },
    parent: footer,
    content: "Facebook",
  });
}
