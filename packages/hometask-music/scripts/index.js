import "../styles/style.css";
import { createElement } from "./createElement";
import setUpFooter from "../components/footer/footer";
import setUpLogo from "../components/logo/logo";
import setUpHeaderStartPage from "../components/startPage/header/headerStartPage";
import setUpMainStartPage from "../components/startPage/main/mainStartPage";
import setUpMainDiscoveryPage from "../components/discoveryPage/main/mainDiscoveryPage";
import setUpHeaderDiscoveryPage from "../components/discoveryPage/header/headerDiscoveryPage";
import setUpHeaderJoinPage from "../components/joinPage/header/headerJoinPage";
import setUpMainJoinPage from "../components/joinPage/main/mainJoinPage";

const app = document.querySelector("#app");

const newHeader = createElement({
  tag: "header",
  parent: app,
});

newHeader.style.position = "relative";

setUpHeaderStartPage(newHeader);

const newMain = createElement({
  tag: "main",
  parent: app,
});

setUpLogo(newHeader);

setUpMainStartPage(newMain);

setUpFooter(app);

function removePageItems() {
  newMain.querySelector(".app__main").remove();
  newHeader.querySelector(".app__header").remove();
  document.querySelector(".main__image").remove();
}

function handleHeaderClick(event) {
  if (event.target.id === "discover") {
    removePageItems();
    setUpHeaderDiscoveryPage(newHeader);
    setUpMainDiscoveryPage(newMain);
  }

  if (
    event.target.id === "join" ||
    event.target.id === "button-join" ||
    event.target.id === "join-discoverPart"
  ) {
    removePageItems();
    setUpHeaderJoinPage(newHeader);
    setUpMainJoinPage(newMain);
  }

  if (event.target.id === "discover-joinPart") {
    removePageItems();
    setUpHeaderStartPage(newHeader);
    setUpMainStartPage(newMain);
  }
}

const { body } = document;

body.addEventListener("click", handleHeaderClick);
newMain.addEventListener("click", handleHeaderClick);
